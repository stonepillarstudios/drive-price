package com.sps.kilorand;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.startapp.android.publish.StartAppSDK;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class LoadingView extends Activity {

    @Override
    public void onCreate(Bundle SIS){
        super.onCreate(SIS);
        setContentView(R.layout.activity_loading_view);

        StartAppSDK.init(this, MainScreen.MyValues.appID, true);   //last param false to disable return ads

        AdView mAdView = (AdView) findViewById(R.id.bigAdView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);


    }

    public String currency = MainScreen.currency;
    public MainScreen.FuelType fuelType = MainScreen.fuelType;
    public String country = MainScreen.country;
    public boolean avgTaken = MainScreen.avgTaken;
    public double transferGasPrice;
    public int convertInt = MainScreen.convertInt;



    public void onStart(){
        super.onStart();

        setLoadingText(getResources().getString(R.string.gettingPrices));

        new GetGasPrice().execute("url is constant");

    }

    public class GetGasPrice extends AsyncTask<String, Integer, Double> {

        public Double doInBackground (String... params){
            String fuelTypeUrl;
            if (fuelType == MainScreen.FuelType.DIESEL){
                fuelTypeUrl = "diesel";
            }else{
                fuelTypeUrl = "gasoline";
            }

            String countryUrl = country.replace(" ", "-");
            if (countryUrl.equals("United-States")){    //exceptions
                countryUrl = "USA";
            }

            Document doc = null;
            try {
                doc = Jsoup.connect("http://www.globalpetrolprices.com/" + countryUrl + "/" + fuelTypeUrl + "_prices/").timeout(15000).get();
            }catch (Exception e){
                System.out.println("jsoup connect/get fail: gas");
                e.printStackTrace();
            }

            if (doc == null){
                return -1.00;
            }

            Elements stuff = doc.getElementsByClass("tipInfo");
            String big = stuff.toString();  //unideal, but text im looking for does not have a tag nor id. :(

            avgTaken = false;
            String priceText = "-1.00";
            int pos;
            if ((pos = big.indexOf("during that period was ")) != -1){  //country found
                pos += "during that period was ".length();
                big = big.substring(pos);
                int posEnd = big.indexOf(" ");
                priceText = big.substring(0, posEnd);
            }else if ((pos = big.indexOf("around the world is ")) != -1){   //country not found, avg used
                pos += "around the world is ".length();
                big = big.substring(pos);
                int posEnd = big.indexOf(" ");
                priceText = big.substring(0, posEnd);
                avgTaken = true;
            }else if ((pos = big.indexOf("Based on these estimates, the price of " + fuelTypeUrl + " is ")) != -1){  //no up to date data on country, est used
                pos += ("Based on these estimates, the price of " + fuelTypeUrl + " is ").length();
                big = big.substring(pos);
                int posEnd = big.indexOf(" ");
                priceText = big.substring(0, posEnd);
            }

            if (priceText.equals("U.S.")){  //issue on their side, just take world avg
                pos = big.indexOf("in the world for this period is ");
                pos += "in the world for this period is ".length();
                big = big.substring(pos);
                int posEnd = big.indexOf(" ");
                priceText = big.substring(0, posEnd);
                avgTaken = true;
            }

            return Double.parseDouble(priceText);
        }

        public void onPostExecute(Double d){
            whenGasGotten(d);
        }

    }

    public void whenGasGotten(Double gasPrice){

        if (gasPrice == -1.00){
            JanTools.makeToast(getResources().getString(R.string.error));
            System.out.println("text not found in par");
            new JanTools().gotoActivity(MainScreen.class);//includes finish
            return;
        }

        transferGasPrice = gasPrice;

        setLoadingText(getResources().getString(R.string.gettingExchange));

        new GetExchangeRate().execute("url is constant");

    }

    public class GetExchangeRate extends AsyncTask<String, Integer, Float> {

        public Float doInBackground (String... params){

            if (currency.equals("USD")){
                return 1f;
            }

            Document doc = null;
            try {
                doc = Jsoup.connect("http://api.fixer.io/latest?base=USD").timeout(15000).ignoreContentType(true).get();
            }catch (Exception e){
                System.out.println("jsoup connect/get fail: exchange");
                e.printStackTrace();
            }

            if (doc == null){
                return -1f;
            }

            String exch;
            String big = doc.toString();
            int pos = big.indexOf(currency);
            pos += 5;
            big = big.substring(pos);
            int posEnd;
            if ((posEnd = big.indexOf(",")) != -1){
                exch = big.substring(0, posEnd);
            }else{
                posEnd = big.indexOf("}");
                exch = big.substring(0, posEnd);
            }

            return Float.parseFloat(exch);
        }

        public void onPostExecute(Float f){
            transferExch = f;

            setLoadingText(getResources().getString(R.string.doingConversions));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    (findViewById(R.id.txtLoading)).setVisibility(View.GONE);
                    (findViewById(R.id.loader)).setVisibility(View.GONE);
                    (findViewById(R.id.btnDone)).setVisibility(View.VISIBLE);

                }
            }, JanTools.getRandomInt(2500, 5000));

        }
    }

    private float transferExch;

    public void onDoneClick(View view) {

        Intent intent = new Intent(getApplicationContext(), MainScreen.class);
        intent.putExtra("exch", LoadingView.this.transferExch);
        intent.putExtra("avgTaken", avgTaken);
        intent.putExtra("transferGasPrice", transferGasPrice);
        intent.putExtra("convertInt", convertInt);

        if (MainScreen.conversionType == MainScreen.ConversionType.CURRENCY){
            intent.putExtra("conversionType", "currency");
        }else{
            intent.putExtra("conversionType", "distance");
        }

        startActivity(intent);  //goto mainscreen with values
        finish();

    }

    @Override
    public void onBackPressed(){/*dont go back*/}

    public void setLoadingText(String text) {
        TextView tv = (TextView)
                findViewById(R.id.txtLoading);
        tv.setText(text);
    }

}

