package com.sps.kilorand;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.startapp.android.publish.StartAppSDK;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle(getResources().getString(R.string.settings));

        StartAppSDK.init(this, MainScreen.MyValues.appID, true);   //last param false to disable return ads

        JanTools.setContext(this);

        String[] currencies = {
                "AUD",
                "BGN",
                "BRL",
                "CAD",
                "CHF",
                "CNY",
                "CZK",
                "DKK",
                "EUR",
                "GBP",
                "HKD",
                "HRK",
                "HUF",
                "IDR",
                "ILS",
                "INR",
                "JPY",
                "KRW",
                "MXN",
                "MYR",
                "NOK",
                "NZD",
                "PHP",
                "PLN",
                "RON",
                "RUB",
                "SEK",
                "SGD",
                "THB",
                "TRY",
                "USD",
                "ZAR",
        };

        Spinner spinner = (Spinner) findViewById(R.id.spnrCurrency);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        String[] locales = Locale.getISOCountries();
        List<String> lands = new ArrayList<>();

        for (String countryCode : locales) {

            Locale obj = new Locale("", countryCode);
            lands.add(obj.getDisplayCountry());
        }

        lands.remove(lands.size() - 1); //remove 'unkown'

        String[] temp = new String[lands.size()];
        temp = lands.toArray(temp);

        Arrays.sort(temp); //alphabetical

        String[] countries = new String[temp.length + 1];
        System.arraycopy(temp, 0, countries, 0, temp.length);
        countries[countries.length - 1] = "Other";

        spinner = (Spinner) findViewById(R.id.spnrCountry);
        adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, countries);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        getWindow().setSoftInputMode(       //hide keyboard on start
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        publicCurrencies = currencies;
        publicCountries = countries;

    }

    public String[] publicCountries;
    public String[] publicCurrencies;

    @Override
    public void onStart(){
        super.onStart();

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle(getResources().getString(R.string.loadingSettings));
        dialog.setCancelable(false);
        dialog.show();

        boolean metric = JanTools.readBool("metric");
        if (metric) {
            ((ToggleButton) findViewById(R.id.tglUnits)).toggle();
            ((TextView) findViewById(R.id.txtConsumption)).setText(getResources().getString(R.string.consumption_metric));
        }

        boolean diesel = JanTools.readBool("diesel");
        if (diesel) {
            ((ToggleButton) findViewById(R.id.tglFuelType)).toggle();
        }

        String efficiency = JanTools.read("efficiency");
        ((EditText) findViewById(R.id.edtConsumption)).setText(efficiency);

        String tcurrency = JanTools.read("currency");
        int pos = 0;
        for (int i = 0; i < publicCurrencies.length; i++) {
            if (publicCurrencies[i].equals(tcurrency)) {
                pos = i;
            }
        }
        Spinner curSpinner = (Spinner) findViewById(R.id.spnrCurrency);
        curSpinner.setSelection(pos);

        String tCountry = JanTools.read("country");
        pos = 0;
        for (int i = 0; i < publicCountries.length; i++) {
            if (publicCountries[i].equals(tCountry)) {
                pos = i;
            }
        }
        Spinner counSpinner = (Spinner) findViewById(R.id.spnrCountry);
        counSpinner.setSelection(pos);

        dialog.hide();
    }

    public void onUnitsClick(View view) {
        boolean checked = ((ToggleButton) view).isChecked();
        JanTools.write("metric", checked + "");
        if (checked){
            ((TextView) findViewById(R.id.txtConsumption)).setText(getResources().getString(R.string.consumption_metric));
        }else{
            ((TextView) findViewById(R.id.txtConsumption)).setText(getResources().getString(R.string.consumption_gay));
        }
    }

    public void onFuelTypeClick(View view) {
        JanTools.write("diesel",
                ((ToggleButton) view).isChecked() + "");
    }

    public void onInfoClick(View view) {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.info_contentDescription))
                .setMessage(getResources().getString(R.string.consumptionExplanation))
                .setNeutralButton(getResources().getString(android.R.string.ok), null)
                .show();
    }

    public void onDoneClick(View view) {
        String selectedCountry = ((Spinner) findViewById(R.id.spnrCountry)).getSelectedItem().toString();
        JanTools.write("country", selectedCountry);

        String inputtedEff = ((EditText) findViewById(R.id.edtConsumption)).getText().toString();
        if (inputtedEff.length() == 0){
            JanTools.makeToast(getResources().getString(R.string.inputConsumption));
            return;
        }
        JanTools.write("efficiency", inputtedEff);

        String selectedCur = ((Spinner) findViewById(R.id.spnrCurrency)).getSelectedItem().toString();
        JanTools.write("currency", selectedCur);

        JanTools.write("configDone", "true");

        new JanTools().gotoActivity(MainScreen.class);
        finish();
    }

    public void onBackPressed(){
        new JanTools().gotoActivity(MainScreen.class);
        finish();
    }
}
